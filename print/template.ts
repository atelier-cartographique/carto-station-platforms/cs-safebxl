import {
  PartialSpec,
  TemplateSpec,
  TemplateCollection,
} from "view/src/components/print/template";

const makeSpec = (s: PartialSpec): TemplateSpec => ({
  fontSize: 10,
  strokeWidth: 1,
  textAlign: "left",
  color: "black",
  ...s,
});

const bigWidthA0 = 1189;
const smallWidthA0 = 841;
const marginA0 = 60;
const mapWidth = smallWidthA0 - marginA0 * 2;

export const templates: TemplateCollection = {
  "a3/portrait": { resolution: 200 },
  "a3/landscape": { resolution: 200 },
  "a4/portrait": {
    resolution: 200,
    title: makeSpec({
      rect: { x: 130, y: 6, width: 74, height: 35 },
      textAlign: "left",
      fontSize: 22,
      color: "#d53e2a",
    }),

    description: makeSpec({
      rect: { x: 130, y: 48, width: 68, height: 78 },
      textAlign: "left",
      fontSize: 9,
      color: "#562821",
    }),

    map: makeSpec({
      rect: { x: 6, y: 6, width: 120, height: 120 },
      color: "#562821",
      strokeWidth: 0.02,
    }),

    legend: makeSpec({
      rect: { x: 6, y: 138, width: 198, height: 138 },
    }),

    legendItem: makeSpec({
      rect: { x: 0, y: 0, width: 62, height: 5 },
      fontSize: 8,
      color: "#562821",
    }),

    attribution: makeSpec({
      rect: { x: 10, y: 124, width: 110, height: 10 },
      fontSize: 5.5,
      color: "#562821",
    }),

    north: makeSpec({
      rect: { x: 120, y: 120, width: 6, height: 6 },
      strokeWidth: 0.5,
      color: "#562821",
    }),

    scaleline: makeSpec({
      rect: { x: 74, y: 116, width: 40, height: 12 },
      strokeWidth: 0.2,
      fontSize: 8,
    }),

    credits: makeSpec({
      rect: { x: 6, y: 128, width: 120, height: 30 / 2 },
      fontSize: 5.5,
      textAlign: "left",
      color: "#562821",
    }),

    logo: makeSpec({ rect: { x: 174, y: 277, width: 30, height: 15 } }),
  },

  "a4/landscape": {
    resolution: 200,
    title: makeSpec({
      rect: { x: 190, y: 6, width: 97, height: 25 },
      textAlign: "left",
      fontSize: 22,
      color: "#d53e2a",
    }),

    description: makeSpec({
      rect: { x: 190, y: 37, width: 90, height: 45 },
      textAlign: "left",
      fontSize: 8,
      color: "#562821",
    }),

    legend: makeSpec({ rect: { x: 190, y: 88, width: 102, height: 100 } }),

    legendItem: makeSpec({
      rect: { x: 0, y: 0, width: 50, height: 5 },
      fontSize: 7,
      color: "#562821",
    }),

    map: makeSpec({
      rect: { x: 6, y: 6, width: 180, height: 180 },
      color: "#562821",
      strokeWidth: 0.02,
    }),

    attribution: makeSpec({
      // rect: { x: 8, y: 200, width: 180, height: 10 },
      rect: { x: 0, y: 0, width: 0, height: 0 },
      fontSize: 5.5,
      color: "#562821",
    }),

    north: makeSpec({
      rect: { x: 178, y: 178, width: 6, height: 6 },
      strokeWidth: 0.5,
    }),

    scaleline: makeSpec({
      rect: { x: 135, y: 175, width: 40, height: 12 },
      strokeWidth: 0.05,
      fontSize: 8,
    }),

    credits: makeSpec({
      rect: { x: 140, y: 198, width: 45, height: 10 },
      fontSize: 5.5,
      textAlign: "left",
      color: "#562821",
    }),
    logo: makeSpec({ rect: { x: 6, y: 190, width: 30, height: 15 } }),
  },

  "a0/portrait": {
    resolution: 100,
    title: makeSpec({
      rect: {
        x: marginA0,
        y: mapWidth + marginA0 + 20,
        width: mapWidth,
        height: 40,
      },
      textAlign: "left",
      fontSize: 60,
      color: "#d53e2a",
    }),

    map: makeSpec({
      rect: {
        x: marginA0,
        y: marginA0,
        width: smallWidthA0 - marginA0 * 2,
        height: smallWidthA0 - marginA0 * 2,
      },
      color: "#562821",
      strokeWidth: 0.1,
    }),

    description: makeSpec({
      rect: {
        x: marginA0,
        y: mapWidth + marginA0 + 30 + 20 + 20,
        width: mapWidth / 4 - 30,
        height: 270,
      },
      textAlign: "left",
      fontSize: 20,
      color: "#562821",
    }),

    legend: makeSpec({
      rect: {
        x: marginA0 + mapWidth / 4,
        y: marginA0 + mapWidth + 30 + 20 + 20,
        width: (mapWidth / 4) * 3,
        height: 270,
      },
    }),

    legendItem: makeSpec({
      rect: {
        x: 0,
        y: 0,
        width: mapWidth / 4,
        height: 20,
      },
      fontSize: 20,
    }),

    attribution: makeSpec({
      rect: {
        x: marginA0,
        y: marginA0 + mapWidth + 5,
        width: 500,
        height: 40,
      },
      fontSize: 12,
    }),

    credits: makeSpec({
      rect: {
        x: marginA0,
        y: mapWidth + marginA0 + 5,
        width: 500,
        height: 40,
      },
      fontSize: 12,
    }),

    north: makeSpec({
      rect: {
        x: mapWidth + marginA0 - 15,
        y: mapWidth + marginA0 + 5,
        width: 15,
        height: 15,
      },
      strokeWidth: 1,
    }),

    scaleline: makeSpec({
      rect: {
        x: 560,
        y: mapWidth + marginA0,
        width: 200,
        height: 20,
      },
      strokeWidth: 0.1,
      fontSize: 12,
    }),

    // credits: makeSpec({
    //   rect: { x: 841 - 30 - 70 - 65 - 20, y: 1135, width: 65, height: 40 },
    //   fontSize: 22,
    //   textAlign: "left",
    // }),

    logo: makeSpec({
      rect: {
        x: smallWidthA0 - marginA0 - 70,
        y: bigWidthA0 - marginA0 / 2 - 35,
        width: 70,
        height: 35,
      },
    }),
  },

  "a0/landscape": {
    resolution: 150,
    title: makeSpec({
      rect: { x: 30 + 781 + 20, y: 30, width: 328, height: 104 },
      textAlign: "left",
      fontSize: 96,
      color: "#562821",
    }),

    map: makeSpec({
      rect: { x: 30, y: 30, width: 781, height: 781 },
      color: "#562821",
      strokeWidth: 0.1,
    }),

    legend: makeSpec({
      rect: {
        x: 30 + 781 + 20,
        y: 30 + 104 + 15,
        width: 328,
        height: 841 - 30 - 104 - 15 - 15 - 40 - 30,
      },
    }),
    legendItem: makeSpec({
      rect: { x: 0, y: 0, width: 272, height: 20 },
      fontSize: 24,
    }),

    attribution: makeSpec({
      rect: { x: 37, y: 30 + 771 - 5, width: 500, height: 40 },
      fontSize: 22,
    }),

    north: makeSpec({
      rect: { x: 787, y: 30 + 750 + 12, width: 15, height: 15 },
      strokeWidth: 1,
    }),

    scaleline: makeSpec({
      rect: { x: 577, y: 30 + 745 + 10, width: 200, height: 27 },
      strokeWidth: 1,
      fontSize: 22,
    }),

    credits: makeSpec({
      rect: {
        x: 30 + 781 + 20,
        y: 841 - 30 - 40 + 5,
        width: 65,
        height: 40,
      },
      fontSize: 22,
      textAlign: "left",
    }),

    logo: makeSpec({
      rect: {
        x: 1189 - 30 - 70,
        y: 841 - 30 - 40 + 10,
        width: 70,
        height: 35,
      },
    }),
  },
};

export default templates;
